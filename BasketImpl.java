import java.util.*;

public class BasketImpl implements Basket {
    public Map<String, Integer> products = new HashMap();

    @Override
    public void addProduct(String product, int quantity) {
        Integer count = products.get(product);
        if (count != null) {
            quantity += count;
        }
        products.put(product, quantity);
    }

    @Override
    public void removeProduct(String product) {
        products.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        products.put(product, quantity);
    }

    @Override
    public void clear() {
        products.clear();
    }

    @Override
    public List<String> getProducts() {
        Set<String> keys = products.keySet();
        List<String> currentProducts = new ArrayList<>();
        currentProducts.addAll(keys);
        return currentProducts;
    }

    @Override
    public int getProductQuantity(String product) {
        Integer quantity = products.get(product);
        if (quantity != null) {
            return quantity;
        }
        return 0;
    }
}
