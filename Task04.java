import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Task04 {
    int findSecondMinimum(int[][][] cube) {
        Set<Integer> allData = new HashSet();
        int max = cube.length - 1;
        int min = 0;

        for (int z = min, y = max, x = min; x <= max; x++, z++, y--) {
            allData.add(cube[z][y][x]);
        }
        for (int z = min, y = max, x = max; z <= max; x--, z++, y--) {
            allData.add(cube[z][y][x]);
        }
        for (int z = max, y = max, x = min; x <= max; x++, z--, y--) {
            allData.add(cube[z][y][x]);
        }
        for (int z = max, y = max, x = max; z >= min; x--, z--, y--) {
            allData.add(cube[z][y][x]);
        }

        return allData.stream().sorted().collect(Collectors.toList()).get(1);
    }
}
