public class Task3 {
    int findElement(int[][] matrix) {
        int length = matrix.length - 1;
        int min = matrix[length][0];

        for (int i = length - 1, j = 1; i >= 0; i--, j++) {
            if (i == j && matrix.length % 2 != 0) {
                continue;
            }
            if (min > matrix[i][j]) {
                min = matrix[i][j];
            }
        }
        return min;
    }
}
