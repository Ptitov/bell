import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        Scanner scanner = new Scanner(System.in);
        int value = -1;

        while (!main.verifyValueMatrix(value)) {
            System.out.println("Введите целочисленное чётное значение размера матрицы больше нуля: ");
            try {
                value = scanner.nextInt();
            } catch (Exception e) {
                value = -1;
            }
        }

        scanner.close();
        int[][] matrix = main.createMatrix(value);
        main.showMatrix(matrix);
    }

    int[][] createMatrix(int size) {
        int startString = 0;
        int endString = size;
        int startField = 0;
        int endField = size;


        int max = size / 2 - 1;
        int[][] matrix = new int[size][size];

        while (startString < endString & startField < endString) {

            for (int i = startString; i < endString; i++) {
                matrix[i][startField] = max;
            }

            startField++;

            for (int j = startField; j < endField; j++) {
                matrix[endString - 1][j] = max;
            }

            endString--;

            for (int i = endString; i >= startString; i--) {
                matrix[i][endField - 1] = max;
            }

            endField--;

            for (int j = endString; j > startString; j--) {
                matrix[startString][j] = max;
            }

            startString++;
            max--;
        }


        return matrix;
    }

    boolean verifyValueMatrix(int value) {
        return value > 0 && value % 2 == 0;
    }

    void showMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + "  ");
            }
            System.out.println();
        }
    }
}

