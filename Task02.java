import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Task02 {
    public static void main(String[] args) {
        Task02 task = new Task02();
        String age = "Возраст";
        String name = "Имя";
        String salary = "Зарплата";
        String endString = "руб";
        List<Map<String, String>> employes = task.createListEmployes();
        List<String> names = employes.stream().filter(o -> Integer.parseInt(o.get(age)) < 30) .map(o->o.get(name)).collect(Collectors.toList());
        System.out.println(names);
        names = employes.stream().filter(o -> o.get(salary).endsWith(endString)) .map(o->o.get(name)).collect(Collectors.toList());
        System.out.println(names);
        System.out.println("Средний возраст всех сотрудников = " + employes.stream().mapToInt(o->Integer.parseInt(o.get(age))).average().orElse(0));
    }

    List<Map<String, String>> createListEmployes() {
        List<Map<String, String>> employes = new ArrayList();
        String name = "Имя";
        String age = "Возраст";
        String position = "Должность";
        String salary = "Зарплата";
        Map<String, String> record1 = new HashMap();
        record1.put(name, "Кирилл");
        record1.put(age, "26");
        record1.put(position, "Middle java dev");
        record1.put(salary, "150000 руб");
        employes.add(record1);
        Map<String, String> record2 = new HashMap();
        record2.put(name, "Виталий");
        record2.put(age, "28");
        record2.put(position, "Senior java automation QA");
        record2.put(salary, "2000$");
        employes.add(record2);
        Map<String, String> record3 = new HashMap();
        record3.put(name, "Александр");
        record3.put(age, "31");
        record3.put(position, "junior functional tester");
        record3.put(salary, "50000 руб");
        employes.add(record3);
        Map<String, String> record4 = new HashMap();
        record4.put(name, "Дементий");
        record4.put(age, "35");
        record4.put(position, "dev-ops");
        record4.put(salary, "1500$");
        employes.add(record4);

        return employes;
    }
}
